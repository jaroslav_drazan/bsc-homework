
Requirements:
=========================================================================================
For requirements specification and my decision regarding them please refer to requirement specification.txt.


How to build:
=========================================================================================
Assumption: Maven (tested with 3.3.1) and Java (at least 1.7) are installed. JAVA_HOME, M2_HOME and PATH (including M2_HOME/bin) specified

cd bsc-homework
mvn clean install

you can also generate javadoc:
mvn javadoc:javadoc

generated jar:
bsc-homework/payment-tracker/target/payment-tracer-1.0.jar

Junit test report:
bsc-homework/payment-tracker/target/surefire-reports

generated javadoc:
bsc-homework/payment-tracker/target/site/apidocs

How to install:
=========================================================================================
Assumption:
at least JRE 1.7 is installed.

No payment-tracer installation is needed. The payment-tracer-1.0.jar is an executable jar with no library dependencies


How to run:
=========================================================================================

java -jar payment-tracer-1.0.jar 
or
java payment-tracer-1.0.jar filePath


Design decisions:
=========================================================================================
This could be a very simple application also implemented as very complex application where almost each component may be abstract and configurable.
I decided not to use any dependency injection framework for simplicity but I designed the application that it can easily be changed to either Spring or other framework supporting dependency injection and inversion of control.
You can find more comments in the code.

A central component is org.bsc.homework.paymenttracer.PaymentTracer class which implements main IO processing and uses all other components. It is launched from org.bsc.homework.paymenttracer.PaymentTracerMain which is a main class.

ThreadSafePaymentManager is a component which implements Thread safety-ness between updating Payment records and reading them. It is also a singleton.


For logging, I use just stderr which can be easily redirected to a file. Of course, Log4j with file rotation would be more appropriate but I used it for simplicity during configuration and user testing.

Packaging - I decided to have just one executable jar for simplicity. Normally, I would split interfaces and implementations to independent jars.

Optional bonus question
=========================================================================================
I did not implement it at the end. If requested, I can of course. This new functionality can be easily changed in the following way:
Implement a new PaymentFormatter class which will have a connection to class / component with exchange rate calculation. The exchange rate would not be then implemented in the Formatter but rather in the component on business layer to avoid presentation and business logic layer mixing.

It could be possible, that it is also not good to implement a call from Formatter to this class. In that case the solution can be extended to support Payment class also with USD amount and use this class as an imput for Formatter. 
Final decision would depend on exact current and possible furture requiremnts and also on how the component providing USD amounts would look like and performance requirements. If it is a simple component, EJB or even 3rd party Webservice.
 
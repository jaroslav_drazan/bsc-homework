package org.bsc.homework.paymenttracer.input;

import org.bsc.homework.paymenttracer.data.Payment;

/**
 * 
 * Implementations of this interface parses given paymentString and convert it to <code>Payment</code>
 *
 * @author Jaroslav Drazan
 */
public interface PaymentParser {
	
	/**
	 * Method parses given <code>paymentString</code> and converts it to <code>Payment</code>
	 * @param paymentString String defining payment
	 * @return parsed string as a <code>Payment</code>.
	 */
	public Payment parsePaymentString(String paymentString);
}

package org.bsc.homework.paymenttracer;

import java.util.Set;

import org.bsc.homework.paymenttracer.data.Payment;

/**
 * Implementations of this interface implements business logic related to collecting payments. 
 * 
 * @author Jaroslav Drazan
 * 
 */
public interface PaymentManager {

	/**
	 * Method adds a <code>payment</code> to already collected payments.
	 * @param payment payment to be collected
	 */
	public void addPayment(Payment payment);

	/**
	 * Method returns collected payments.
	 * @return collected payments
	 */
	public Set<Payment> getPayments();
	
	/**
	 * Method cleans stored payments. Method is used for testing only
	 */
	public void clear();

}

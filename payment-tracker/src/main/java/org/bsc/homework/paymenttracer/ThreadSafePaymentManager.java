package org.bsc.homework.paymenttracer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bsc.homework.paymenttracer.data.Payment;

/**
 * This class implements business logic related to collecting payments. 
 * As I do not use Spring nor any other Dependency Injection framework, 
 * I decided for simplicity to implement it as a singleton.
 * Of course, if some dependency injection framework were used, 
 * than the Singleton part is not needed. 
 * The advantage of such approach would be loose coupling done only on configuration level
 *  which enable simple switch of this class implementation.
 * I can imagine there could be different strategies used to add payments and more business logic.
 *  But it is out of scope.
 *  
 *  This class is thread safe. Only a single thread can add Payment 
 *  and It returns shallow copies with Payments which are immutable, 
 *  i.e., thread safe too.
 *  
 *  The risk here could be performance in case there were a lot of R/W requests concurrently. 
 *  But for synchronization one minute read and manual input, it is more than sufficient.
 *  
 *  @author Jaroslav Drazan
 *
 */
public class ThreadSafePaymentManager implements PaymentManager {
	
	private static ThreadSafePaymentManager INSTANCE = new ThreadSafePaymentManager();

	private Map<String, Payment> payments = new HashMap<String, Payment>();
	
	//Singleton, no instances can be created
	private ThreadSafePaymentManager() {
	}
	
	/**
	 * Factory method which returns a singleton instance
	 * @return a singleton instance
	 */
	public static ThreadSafePaymentManager getInstance() {
		return INSTANCE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void addPayment(Payment payment) {
		if(payment!= null && payment.getCurrency()!= null) {
			Payment oldPayment = payments.get(payment.getCurrency());
			if(oldPayment!= null) {
				if (payment.getAmount()!= 0) {
					int newAmount = payment.getAmount() + oldPayment.getAmount();
					if(newAmount!= 0) {
						payments.put(payment.getCurrency(), new Payment(payment.getCurrency(), newAmount));
					} else {
						payments.remove(payment.getCurrency());
					}
				} // if new amount is 0, keep the original value
			} else if (payment.getAmount()!= 0) {
				//payment is immutable so I can put the same instance I received and it is thread safe
				payments.put(payment.getCurrency(), payment);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized Set<Payment> getPayments() {
		return new HashSet<Payment>(payments.values());
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void clear() {
		payments.clear();
	}

}

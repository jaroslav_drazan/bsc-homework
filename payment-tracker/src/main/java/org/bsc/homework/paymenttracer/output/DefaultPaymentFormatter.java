package org.bsc.homework.paymenttracer.output;

import org.bsc.homework.paymenttracer.data.Payment;

/**
 * 
 * Default Implementation of PaymentFormatter interface. It converts <code>Payment</code> to printable string as required in specification:
 * Program output:
 * Currency-Code amount
 * 
 * Sample currency code amounts:
 * USD 900
 * RMB 2000
 * HKD 300
 * 
 * @author Jaroslav Drazan
 */
public class DefaultPaymentFormatter implements PaymentFormatter {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String formatPayment(Payment payment) {
		StringBuffer sb = new StringBuffer();
		
		if(payment!= null) {
			//this class is not responsible for null validations so it formats null currencies as an empty string.
			String printCurrency = payment.getCurrency() == null ? "" : payment.getCurrency();
			sb.append(printCurrency).append(' ').append(payment.getAmount());
		}
		
		return sb.toString();
	}

}

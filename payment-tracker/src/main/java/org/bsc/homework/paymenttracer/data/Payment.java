package org.bsc.homework.paymenttracer.data;

/**
 * Immutable value object keeping payments (ammount and currency)
 * 
 * The object is immutable to be thread safe.
 * 
 * In some more complex solution, currency could also be a class 
 * which can encapsulate either other data such as name, state,... 
 * or it can contain some additional methods such as convertToOtherCurrency.
 * All solutions has their pros and cons. I prefer usually to split (decouple) business data 
 * and complex business logic which depend on other business data to independent classes / components (e.g., stateless SessionBeans and Entities / value objects). 
 * Logic independent on data enables to implement strategies and simply changing business logic on one hand and on the other hand to make data easily persistent. 
 * It may also make unit testing easier. But it makes the solution more complex to understand.
 * 
 * @author Jaroslav Drazan
 *
 */
public class Payment {

	// ======================================
	// = Attributes =
	// ======================================

	private String currency;
	private int amount;
	
	// ======================================
	// = Constructors =
	// ======================================

	/**
	 * Constructor
	 * @param currency currency code in format 3 capital characters.
	 * @param amount amount of currency
	 */
	public Payment(String currency, int amount) {
		this.currency = currency;
		this.amount = amount;
	}

	// ======================================
	// = Getters =
	// ======================================
	
	public String getCurrency() {
		return currency;
	}

	public int getAmount() {
		return amount;
	}

	

	// ======================================
	// = hash, equals, toString =
	// ======================================


	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("Payment");
		sb.append("{ currency=").append(currency);
		sb.append(", amount=").append(amount);
		sb.append('}');
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + amount;
		result = prime * result
				+ ((currency == null) ? 0 : currency.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Payment other = (Payment) obj;
		if (amount != other.amount)
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		return true;
	}
}
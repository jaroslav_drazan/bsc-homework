package org.bsc.homework.paymenttracer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import org.bsc.homework.paymenttracer.data.Payment;
import org.bsc.homework.paymenttracer.input.DefaultPaymentParser;
import org.bsc.homework.paymenttracer.input.PaymentParser;
import org.bsc.homework.paymenttracer.output.DefaultPaymentFormatter;
import org.bsc.homework.paymenttracer.output.PaymentFormatter;
import org.bsc.homework.paymenttracer.output.PaymentPrinter;
import org.bsc.homework.paymenttracer.output.StdoutPaymentPrinter;

/**
 * This class encapsulates I/O processing and is a controller of the PaymentTracer application.
 * 
 * 
 * @author Jaroslav Drazan
 */
public class PaymentTracer {
	
	//for logging purposes only using default locale
	private DateFormat timeFormatter = SimpleDateFormat.getTimeInstance();
	
	
	//If Spring or JEE or other DependencyInjection mechanism were used, 
	//these references can be specified in configuration and not hardcoded.
	private PaymentManager pm = ThreadSafePaymentManager.getInstance();
	private PaymentParser parser = new DefaultPaymentParser();
	private PaymentPrinter printer = new StdoutPaymentPrinter();
	private PaymentFormatter formatter = new DefaultPaymentFormatter();
	
	
	/**
	 * Method reads <code>inputFile</code> and processes it as required by specification using configured <code>parser</code>.
	 * @param inputFile file to be parsed
	 */
	public void processInputFile(String inputFile) {
		if(inputFile!= null) {
			File in = new File(inputFile);
			if(in.isFile()) {
				System.err.println("DEBUG: input file found, starting to process");
				//using FileReader in this way means using default encoding,
				//there are no requirements to specify any.
				try(BufferedReader ir = new BufferedReader(new FileReader(in));) {
					
					String line = null;
					while((line = ir.readLine() )!= null) {
						
						processPaymentLineFromFile(line);
					}
					
				} catch (IOException e) {
					//ignore errors, just log them to stderr
					e.printStackTrace();
				}
				System.err.println("DEBUG: file processed");
			} else {
				System.err.println("DEBUG: file not found or is not a file: " + inputFile);
			}
		}
	}



	/**
	 * Method processes line with a user input from file and parses it with configured <code>parser</code>. 
	 * @param line line read from file
	 */
	protected void processPaymentLineFromFile(String line) {
		Payment payment = parser.parsePaymentString(line);
		if(payment!= null) {
			pm.addPayment(payment);
		} else {
			System.err.println("DEBUG: file processing - invalid line found: " + line);
		}
	}

	/**
	 * Method processes line with a user input and parses it with configured <code>parser</code>. 
	 * @param line line read from stdin
	 * @return true if continue processing next line
	 */
	protected boolean processPaymentLineFromStdIn(String line) {
		boolean rv = true;
		//command can be configured. If more commands are available, I can consider Command design pattern to be used.
		if("quit".equals(line)) {
			rv = false;
		} else {
			Payment payment = parser.parsePaymentString(line);
			if(payment!= null) {
				pm.addPayment(payment);
			} else {
				System.err.println("DEBUG: input processing - invalid line entered: " + line);
			}
		}
		return rv;
	}
	
	/**
	 * Method reads stdin and processes it as required by specification using configured <code>parser</code>.
	 */
	public void processStdIn() {
		System.err.println("DEBUG: starting reading stdin. Please enter quit or Currency-Ammount");
		//using FileReader in this way means using default encoding,
		//there are no requirements to specify any.
		try(BufferedReader ir = new BufferedReader(new InputStreamReader(System.in));) {
			
			String line = null;
			while((line = ir.readLine() )!= null ) {
				
				//Could be included in while condition 
				//but I do not like methods with side effects (not idempotent) to be called in 
				//condition clauses except few exceptions such as reading from stream as above
				if(!processPaymentLineFromStdIn(line)) {
					break;
				}
				
			}
			
		} catch (IOException e) {
			//ignore errors, just log them to stderr
			e.printStackTrace();
		}
		System.err.println("DEBUG: Reading from stdin stopped.");
	}
	
	/**
	 * Method prints existing payments as required by specification using configured <code>printer</code> and <code>formatter</code>.
	 */
	public void tracePayments() {
		System.err.println("Debug time: " + timeFormatter.format(new Date()));
		Set<Payment> payments = pm.getPayments();
		if(payments!= null) {
			printer.print(payments, formatter);
		} else {
			System.err.println("Debug: no payments found");
		}
	}
	
	/**
	 * Method clears existing payments. It is used for testing only
	 */
	public void clearPayments() {
		pm.clear();
	}
	
	/**
	 * Method returns existing payments. It is used for testing only
	 * @return existing payment
	 */
	public Set<Payment> getPayments() {
		return pm.getPayments();
	}

}

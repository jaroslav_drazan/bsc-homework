package org.bsc.homework.paymenttracer;

/**
 * Main class. This class is called from commandline and processes given command line arguments.
 *
 *
 * @author Jaroslav Drazan
 */
public class PaymentTracerMain {
	
	/**
	 * Starts application
	 * @param args - command line arguments. Application supports one optional parameter which is a file path.
	 */
	public static void main(String[] args) {
		if(args.length > 1) {
			System.err.println("Usage: PaymentTracerMain [filePath]");
			System.exit(1);
		}
		
		final PaymentTracer tracer = new PaymentTracer();
		if(args.length == 1) {
			tracer.processInputFile(args[0]);
		} 

		//The code could be extracted to a class but this is so simple that I used inner class
		Thread tracingThread = new Thread( new Runnable() {
			
			@Override
			public void run() {
				System.err.println("Debug: Tracing started");
				
				while(true) {
					
					tracer.tracePayments();
					
					try {
						//wait a minute. This could/should be made configurable
						Thread.sleep(60000);
					} catch (InterruptedException e) {
						//log error and continue
						e.printStackTrace();
					}
					
				}
				
			}
		});
		
		//this thread does not block application shut down
		tracingThread.setDaemon(true);
		tracingThread.start();
		
		//Start reading stdin. It is a blocking call
		tracer.processStdIn();
		System.err.println("Debug: Application terminated");
	}
}

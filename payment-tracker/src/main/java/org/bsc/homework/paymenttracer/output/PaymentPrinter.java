package org.bsc.homework.paymenttracer.output;

import java.util.Set;

import org.bsc.homework.paymenttracer.data.Payment;

/**
 * Implementations of this interface prints payments.
 *
 *
 * @author Jaroslav Drazan
 */
public interface PaymentPrinter {
	
	/**
	 * Prints <code>payments</code> to output. Each payment is formatted by <code>paymentFormatter</code>.
	 * @param payments payments to be printed
	 * @param paymentFormatter formatter which formats each payment.
	 */
	public void print(Set<Payment> payments, PaymentFormatter paymentFormatter);

}

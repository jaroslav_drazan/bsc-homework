package org.bsc.homework.paymenttracer.input;

import org.bsc.homework.paymenttracer.data.Payment;

/**
 * 
 * Default Implementation of PaymentParser interface. It parses paymentString from format as specified in specification:
 *
 * 1) Currency-Code: 3 letter uppercase code, such as USD, HKD, RMB, NZD, GBP etc. 
 * 2) Currency-Code-Amount: Currency-Code INTEGER
 * 
 * Sample Currency-Code-Amount:
 * USD 1000
 * HKD 100
 * USD -100
 * RMB 2000
 * HKD 200
 * 
 * @author Jaroslav Drazan
 */
public class DefaultPaymentParser implements PaymentParser {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Payment parsePaymentString(String paymentString) {
		Payment rv = null;
		if(paymentString != null && paymentString.length() > 4 && paymentString.charAt(3) == ' ') {
			
			String currency = paymentString.substring(0,3);
			
			for(int i = 0; i < currency.length(); i++) {
				if(!Character.isUpperCase(currency.charAt(i))) {
					logInvalidFormat();
					System.err.println("Currency is not 3 capital letters");
					currency = null;
					break;
				}
			}
			
			if(currency!= null) {
				//trim - be not strict and tolerate blank spaces at the end of integer (normally it should be specified if we are strict)
				String amountString = paymentString.substring(4).trim(); 
				
				try {
					
					int amount = Integer.parseInt(amountString);
					rv = new Payment(currency, amount);
				} catch (NumberFormatException e) {
					logInvalidFormat();
					System.err.println(e.getMessage());
				}
			}
			
		}  else {
			logInvalidFormat();
		}
		return rv;
	}

	//For simplicity, I do not specify more detailed error messages and log to stderr
	private void logInvalidFormat() {
		System.err.println("paymentString is not in expected format. Expected format is XYZ integer");
	}
		
}

	

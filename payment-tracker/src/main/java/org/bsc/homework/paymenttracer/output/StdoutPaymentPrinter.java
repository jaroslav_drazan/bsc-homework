package org.bsc.homework.paymenttracer.output;

import java.util.Set;

import org.bsc.homework.paymenttracer.data.Payment;

/**
 * 
 * Default Implementation of PaymentPrinter interface. It prints a set with <code>Payment</code> to stdin using given <code>paymentFormatter</code>
 * 
 * @author Jaroslav Drazan
 */
public class StdoutPaymentPrinter implements PaymentPrinter {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(Set<Payment> payments, PaymentFormatter paymentFormatter) {
	
		if(payments!= null && paymentFormatter!= null) {
			for(Payment payment: payments) {
				System.out.println(paymentFormatter.formatPayment(payment));
			}
		}
	}
}

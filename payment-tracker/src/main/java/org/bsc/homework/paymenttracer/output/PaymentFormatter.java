package org.bsc.homework.paymenttracer.output;

import org.bsc.homework.paymenttracer.data.Payment;

/**
 * 
 * Implementations of this interface format payment to printable string.
 * 
 * @author Jaroslav Drazan
 *
 */
public interface PaymentFormatter {
	
	/**
	 * Method convers given <code>payment</code> to printable string
	 * @param payment payment to be converted.
	 * @return printable string
	 */
	public String formatPayment(Payment payment);
	
}

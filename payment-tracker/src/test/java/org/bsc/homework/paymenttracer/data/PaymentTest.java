package org.bsc.homework.paymenttracer.data;

import org.bsc.homework.paymenttracer.data.Payment;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 *  This class tests Payment.equals method. Hashcode method can be tested as well but I do not do it for simplicity.
 *         
 *  @author Jaroslav Drazan
 */
public class PaymentTest {

  // ======================================
  // =              Methods               =
  // ======================================

  @Test
  public void equalsTrue() {

    Payment p1 = new Payment("CZE", -5);
    Payment p2 = new Payment("CZE", -5);
    
    assertTrue(p1.equals(p2));
    assertTrue(p2.equals(p1));
  }
  
  @Test
  public void equalsFalseCurrency() {

    Payment p1 = new Payment("CZE", -5);
    Payment p2 = new Payment("USD", -5);
    
    assertTrue(!p1.equals(p2));
    assertTrue(!p2.equals(p1));
  }
  
  @Test
  public void equalsFalseAmount() {

    Payment p1 = new Payment("CZE", -5);
    
    Payment p2 = new Payment("CZE", 22);
    
    assertTrue(!p1.equals(p2));
    assertTrue(!p2.equals(p1));
  }
  
  
  @Test
  public void equalsFalseCurrencyAmount() {

    Payment p1 = new Payment("CZE", -5);
    Payment p2 = new Payment("EUR", 22);
    
    assertTrue(!p1.equals(p2));
    assertTrue(!p2.equals(p1));
  }
  
  @Test
  public void equalsFalseNull() {

    Payment p1 = new Payment("CZE", -5);
    
    assertTrue(!p1.equals(null));
  }
}
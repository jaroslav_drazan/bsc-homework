package org.bsc.homework.paymenttracer.output;

import static org.junit.Assert.assertEquals;

import org.bsc.homework.paymenttracer.data.Payment;
import org.junit.Test;

/**
 * @author Jaroslav Drazan
 */
public class PaymentFormatterTest {
	
	DefaultPaymentFormatter formatter = new DefaultPaymentFormatter();
	
	@Test
	public void formatPaymentNull() {
		String result = formatter.formatPayment(null);
		assertEquals("", result);
	}
	
	@Test
	public void formatPaymentNullCurrency() {
		String result = formatter.formatPayment(new Payment(null, 100));
		assertEquals(" 100", result);
	}
	
	@Test
	public void formatPaymentPositiveAmount() {
		String result = formatter.formatPayment(new Payment("USD", 505));
		assertEquals("USD 505", result);
	}
	
	@Test
	public void formatPaymentNegativeAmount() {
		String result = formatter.formatPayment(new Payment("CZE", -6));
		assertEquals("CZE -6", result);
	}
	
}

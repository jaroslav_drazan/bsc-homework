package org.bsc.homework.paymenttracer;

import java.util.Set;

import org.bsc.homework.paymenttracer.data.Payment;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * 
 *This class tests basic funcionality of PaymentTracer. 
 *
 *It does not test methods related to file and stdin. 
 *If these methods should be tested then an easy way is to refactor them 
 *so they accept streams only and test the version with streams.
 *
 *@author Jaroslav Drazan
 *
 */
public class PaymentTracerTest {
	
	PaymentTracer tracer = new PaymentTracer();
	
	@Test
	public void processPaymentLineFromFileOk() {
		tracer.processPaymentLineFromFile("CZE 5");

		Set<Payment> payments = tracer.getPayments();
		assertNotNull(payments);
		assertEquals(1, payments.size());
		
		Payment p1 = new Payment("CZE", 5);
		
		assertTrue(payments.contains(p1));
	}
	
	@Test
	public void processPaymentLineFromFileKo() {
		tracer.processPaymentLineFromFile("Cze 5");

		Set<Payment> payments = tracer.getPayments();
		assertNotNull(payments);
		assertEquals(0, payments.size());
	}
	
	
	@Test
	public void processPaymentLineFromStdInOk() {
		tracer.processPaymentLineFromStdIn("CZE 5");

		Set<Payment> payments = tracer.getPayments();
		assertNotNull(payments);
		assertEquals(1, payments.size());
		
		Payment p1 = new Payment("CZE", 5);
		
		assertTrue(payments.contains(p1));
	}
	
	@Test
	public void processPaymentLineFromStdInKo() {
		tracer.processPaymentLineFromStdIn("USD");

		Set<Payment> payments = tracer.getPayments();
		assertNotNull(payments);
		assertEquals(0, payments.size());
	}
	
	@Test
	public void getPaymentsOk() {
		tracer.processPaymentLineFromStdIn("CZE 5");
		tracer.processPaymentLineFromStdIn("USD -25");
		

		Set<Payment> payments = tracer.getPayments();
		assertNotNull(payments);
		assertEquals(2, payments.size());
		
		Payment p1 = new Payment("CZE", 5);
		Payment p2 = new Payment("USD", -25);
		
		assertTrue(payments.contains(p1));
		assertTrue(payments.contains(p2));
	}
	
	@Test
	public void clearPaymentsOk() {
		tracer.processPaymentLineFromStdIn("CZE 5");
		tracer.processPaymentLineFromStdIn("USD 25");
		

		Set<Payment> payments = tracer.getPayments();
		assertNotNull(payments);
		assertEquals(2, payments.size());
		
		Payment p1 = new Payment("CZE", 5);
		Payment p2 = new Payment("USD", 25);
		
		assertTrue(payments.contains(p1));
		assertTrue(payments.contains(p2));
		
		tracer.clearPayments();
		payments = tracer.getPayments();
		
		assertNotNull(payments);
		assertEquals(0, payments.size());
		
	}
	
	
	@After
	public void afterTest(){
		tracer.clearPayments();
	}
	
}

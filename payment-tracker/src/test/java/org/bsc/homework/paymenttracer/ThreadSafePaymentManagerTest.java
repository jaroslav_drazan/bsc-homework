package org.bsc.homework.paymenttracer;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;


import java.util.Set;

import org.bsc.homework.paymenttracer.data.Payment;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Jaroslav Drazan
 */
public class ThreadSafePaymentManagerTest {

	private ThreadSafePaymentManager pm;

	@Before
	public void beforeTest() {
		pm = ThreadSafePaymentManager.getInstance();
	}

	@Test
	public void addPaymentZero() {

		Payment p1 = new Payment("CZE", 0);
		pm.addPayment(p1);
		assertTrue(pm.getPayments().size() == 0);
	}

	@Test
	public void addPaymentTwoDifferentCurrencies() {

		Payment p1 = new Payment("CZE", -5);
		Payment p2 = new Payment("USD", 10);

		pm.addPayment(p1);
		pm.addPayment(p2);

		Set<Payment> payments = pm.getPayments();

		assertNotNull(payments);
		assertTrue(payments.size() == 2);
		assertTrue(payments.contains(p1));
		assertTrue(payments.contains(p2));
	}

	@Test
	public void addPaymentTwoSameCurrencies() {

		Payment p1 = new Payment("CZE", -5);
		Payment p2 = new Payment("CZE", 10);

		pm.addPayment(p1);
		pm.addPayment(p2);

		Set<Payment> payments = pm.getPayments();

		assertNotNull(payments);
		assertTrue(payments.size() == 1);
		assertTrue(payments.contains(new Payment("CZE", 5)));
	}

	@Test
	public void addPaymentTwoSameCurrenciesSumZero() {

		Payment p1 = new Payment("USD", -5);
		Payment p2 = new Payment("USD", 5);

		pm.addPayment(p1);
		pm.addPayment(p2);

		Set<Payment> payments = pm.getPayments();
		assertNotNull(payments);
		assertTrue(payments.size() == 0);
	}
	
	@Test
	public void clear() {

		Payment p1 = new Payment("CZE", -5);
		Payment p2 = new Payment("USD", 10);

		pm.addPayment(p1);
		pm.addPayment(p2);

		Set<Payment> payments = pm.getPayments();

		assertNotNull(payments);
		assertTrue(payments.size() == 2);
		pm.clear();
		
		payments = pm.getPayments();
		assertNotNull(payments);
		assertTrue(payments.size() == 0);
	}
	
	@After
	public void afterTest() {
		pm.clear();
	}
}
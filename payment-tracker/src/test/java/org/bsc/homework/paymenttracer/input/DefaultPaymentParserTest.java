package org.bsc.homework.paymenttracer.input;

import org.bsc.homework.paymenttracer.data.Payment;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * @author Jaroslav Drazan
 */
public class DefaultPaymentParserTest {
	
	private DefaultPaymentParser pp = new DefaultPaymentParser();
	
	@Test
	public void parsePaymentStringOkPositive() {
		Payment result = pp.parsePaymentString("USD 506");
		assertEquals(new Payment("USD",506), result);
	}
	
	@Test
	public void parsePaymentStringOkNegative() {
		Payment result = pp.parsePaymentString("CZE -506");
		assertEquals(new Payment("CZE",-506),result);
	}
	
	@Test
	public void parsePaymentStringOkZero() {
		Payment result = pp.parsePaymentString("CZE 0");
		assertEquals(new Payment("CZE",0), result);
	}
	
	@Test
	public void parsePaymentStringOkPositiveBlankSpacesTail() {
		Payment result = pp.parsePaymentString("CZE 100   	");
		assertEquals(new Payment("CZE",100), result);
	}
	
	@Test
	public void parsePaymentStringKoSeparator() {
		Payment result = pp.parsePaymentString("CZE-0");
		assertNull(result);
	}
	
	@Test
	public void parsePaymentStringKoCurrency() {
		Payment result = pp.parsePaymentString("Cze 0");
		assertNull(result);
	}
	
	@Test
	public void parsePaymentStringKoAmountLetters() {
		Payment result = pp.parsePaymentString("CZE 0fx");
		assertNull(result);
	}
	
	@Test
	public void parsePaymentStringKoAmountLong() {
		Payment result = pp.parsePaymentString("CZE 10000000000000000000000000000000");
		assertNull(result);
	}
	
	@Test
	public void parsePaymentStringKoShort() {
		Payment result = pp.parsePaymentString("CZE ");
		assertNull(result);
		
		result = pp.parsePaymentString("CZE");
		assertNull(result);
		
		result = pp.parsePaymentString("CZ");
		assertNull(result);
	}

}
